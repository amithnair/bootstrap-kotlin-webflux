package com.bootstrap.service.handler

import com.bootstrap.service.model.Item
import com.bootstrap.service.repository.ItemRepository
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.BodyInserters.fromObject
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

class ItemHandler(var repository: ItemRepository) {

    fun save(item: Item): Mono<ServerResponse> {
        try {
            repository.save(item)
        } catch (re: Exception) {
            return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }

        return ServerResponse.ok().build()
    }

//    Flux<Item> items = itemRepository.findAll();
//    return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(items, Item.class);

}