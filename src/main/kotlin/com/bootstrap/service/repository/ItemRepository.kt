package com.bootstrap.service.repository

import com.bootstrap.service.model.Item
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ItemRepository : ReactiveCrudRepository<Item, String>