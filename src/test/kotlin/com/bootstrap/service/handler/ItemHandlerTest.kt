package com.bootstrap.service.handler

import com.bootstrap.service.model.Item
import com.bootstrap.service.repository.ItemRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension


@ActiveProfiles("test")
@ExtendWith(SpringExtension::class)
@SpringBootTest
class ItemHandlerTest {


    @Test
    fun findAllShouldReturnInternalServerErrorOnDBError() {
        val item = Item("1234", "item")

        val mockRepository: ItemRepository = mock()
        whenever(mockRepository.save(item)).thenThrow(RuntimeException("db connection error"))
        val handler = ItemHandler(mockRepository)

        val serverResponseMono = handler.save(item)
        serverResponseMono.map { assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, it.statusCode()) }.block()
    }

    @Test
    fun findAllShouldReturnStatusOkOnSaveSuccess() {
        val item = Item("1234", "item")

        val mockRepository: ItemRepository = mock()
        val mono = null
        whenever(mockRepository.save(item)).thenReturn(mono)
        val handler = ItemHandler(mockRepository)

        val serverResponseMono = handler.save(item)
        serverResponseMono.map { assertEquals(HttpStatus.OK, it.statusCode()) }.block()
    }

    @Test
    fun findAllShouldReturnEmptyPayloadIfNoItemsExist() {
        val item = Item("1234", "item")

        val mockRepository: ItemRepository = mock()
        val mono = null
        whenever(mockRepository.save(item)).thenReturn(mono)
        val handler = ItemHandler(mockRepository)

        val serverResponseMono = handler.save(item)
        serverResponseMono.map {
            assertEquals(HttpStatus.OK, it.statusCode())
        }.block()

    }

    //    @Test
//    fun shouldBeAbleSaveItem() {
//        val item = Item("1234", "first item")
//
//        val mockRepository: ItemRepository = mock()
//        whenever(mockRepository.save(item)).doReturn(Mo)
//        val handler = ItemHandler(mockRepository)
//    }
}
